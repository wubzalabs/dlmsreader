package org.wubzalabs;


import java.io.Console;
import java.io.IOException;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.nio.charset.IllegalCharsetNameException;
import java.nio.charset.UnsupportedCharsetException;
import java.util.List;
import java.util.Scanner;
import java.util.ServiceLoader;

import org.openmuc.jdlms.client.*;
import org.openmuc.jdlms.client.hdlc.HdlcAddress;
import org.openmuc.jdlms.client.hdlc.HdlcClientConnectionSettings;
import org.openmuc.jdlms.client.ip.TcpClientConnectionSettings;
import org.openmuc.jdlms.client.ClientConnectionSettings.ReferencingMethod;
import org.openmuc.jdlms.client.ClientConnectionSettings.Authentication;

/**
* Created by mwilke01 on 10/21/2015.
*/
public class DlmsExample {

    protected InetSocketAddress smartMeter = null;
    protected String password;
    protected String outstationNum;

    public static void main(String[] args) throws UnknownHostException, InterruptedException {
        // Settings to connect to smart meter
        // - IP Destination: 192.168.200.25 (may vary)
        // - TCP Port: 4059 (DLMS standard)
        // - WPort of smart meter: 1 (Management Logical Device)
        // - WPort of client: 16 (Public client)
        // - Referencing: Logical Name

        DlmsExample de = new DlmsExample();
        de.Run();
    }

    public void Run() {

        if (ConsoleInput()) {
            System.out.println("SMARTMETER: " + smartMeter.toString());
            TcpClientConnectionSettings settings = new TcpClientConnectionSettings(smartMeter,
                    1, 1, ReferencingMethod.LN);
            //settings.setAuthentication(Authentication.LOW);
            IClientConnectionFactory factory = ClientConnectionSettings.getFactory();
            IClientConnection conn = null;
            try {
                conn = factory.createClientConnection(settings);
                // Waiting 3s until connect attempt is canceled
                conn.connect(3000);
                System.out.println("\nConnected to smart meter\n");
                // Get parameter to read current time
                // - Interface class: 8 (Clock)
                // - Obis code: 0:0:1:0:0:255 (current time)
                // - Attribute ID: 2 (time)
                GetRequest getClock = new GetRequest(8, new ObisCode(0, 0, 1, 0, 0,
                        255), 2);
                // Read the current time from the remote device
                // Waiting 1s until aborting connection
                List<GetResult> getResults = conn.get(1000, getClock);
                GetResult getClockResult = getResults.get(0);
                System.out.println("\nRead time from smart meter");
                if (getClockResult.isSuccess()) {
// According to IEC 62056-62, the current time will be transmitted
// as byte array
                    System.out.println("Time: " + getClockString(getClockResult
                            .getResultData().getByteArray()));
                } else {
                    System.out.println("Error on reading time. ErrorCode: "
                            + getClockResult.getResultCode());
                }
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (conn != null && conn.isConnected()) {
                    conn.disconnect();
                    System.out.println("\nDisconnected from smart meter\n");
                }
            }
        } else {
            System.out.println("ERROR OCCURRED!");
        }
    }
/*
InetSocketAddress smartMeter = new InetSocketAddress(Inet4Address.getByName("86.188.207.101"), 45859);
TcpClientConnectionSettings settings = new TcpClientConnectionSettings(smartMeter,
1, 16, ReferencingMethod.LN);
settings.setAuthentication(ClientConnectionSettings.Authentication.LOW);
IClientConnectionFactory factory = ClientConnectionSettings.getFactory();
IClientConnection connection = null;
try {
connection = factory.createClientConnection(settings);
// Waiting 3s until connect attempt is canceled
connection.connect(3000, "20871094".getBytes("US-ASCII"));
System.out.println("\nConnected to smart meter\n");
// Get parameter to read current time
// - Interface class: 8 (Clock)
// - Obis code: 0:0:1:0:0:255 (current time)
// - Attribute ID: 2 (time)
GetRequest getClock = new GetRequest(8, new ObisCode(0, 0, 1, 0, 0,
255), 2);
// Read the current time from the remote device
// Waiting 1s until aborting connection
List<GetResult> getResults = connection.get(1000, getClock);
GetResult getClockResult = getResults.get(0);
System.out.println("\nRead time from smart meter");
if (getClockResult.isSuccess()) {
// According to IEC 62056-62, the current time will be transmitted
// as byte array
System.out.println("Time: " + getClockString(getClockResult
.getResultData().getByteArray()));
} else {
System.out.println("Error on reading time. ErrorCode: "
+ getClockResult.getResultCode());
}
} catch (IOException e) {
e.printStackTrace();
} finally {
if (connection != null && connection.isConnected()) {
connection.disconnect();
System.out.println("\nDisconnected from smart meter\n");
}
}


HdlcClientConnectionSettings settings = new HdlcClientConnectionSettings("/dev/ttyUSB0", new HdlcAddress(18),
new HdlcAddress(16), ReferencingMethod.LN);
settings.setBaudrate(9600).setUseHandshake(false).setAuthentication(ClientConnectionSettings.Authentication.LOW);

IClientConnectionFactory factory = ServiceLoader.load(IClientConnectionFactory.class).iterator().next();
IClientConnection connection = null;
try {
connection = factory.createClientConnection(settings);

System.out.print("Enter the password to connect with the smart meter: ");
byte pw[] = null;
if (System.console() != null) {
char[] input = System.console().readPassword();
CharBuffer buffer = CharBuffer.wrap(input);
Charset ascii = Charset.forName("US-ASCII");
ByteBuffer bytes = ascii.encode(buffer);
pw = new byte[bytes.limit()];
bytes.get(pw);
} else {
Scanner scanner = new Scanner(System.in);
String input = scanner.nextLine();
pw = input.getBytes("US-ASCII");
}

connection.connect(3000, pw);
System.out.println("\nConnected to smart meter\n");

GetRequest getSAP = new GetRequest(15, new ObisCode(0, 0, 40, 0, 0, 255), 2);

List<GetResult> getResults = connection.get(10000, false, getSAP);
if (getResults.get(0).isSuccess()) {
printResult(getResults.get(0));
} else {
System.out.println("Error on reading object list: " + getResults.get(0).getResultCode());
}

} catch (IOException e) {
e.printStackTrace();
} finally {
if (connection != null && connection.isConnected()) {
connection.disconnect(false);
System.out.println("\nDisconnected from smart meter\n");
}
}
*/

    private boolean ConsoleInput() {
        String inStg;
        InetAddress meterAddr = null;
        int portNum = 0;
        Scanner input = new Scanner(System.in);

        System.out.print("Enter the Smart Meter Hostname/IP Address: ");
        inStg = input.nextLine();
        if (inStg.length() > 0) {
            try {
                meterAddr = Inet4Address.getByName(inStg);
            } catch (UnknownHostException uhe) {
                System.out.println("--- INVALID HOST: " + inStg);
                System.out.println("--- Error: " + uhe.getMessage());
                return false;
            } catch (Exception e) {
                System.out.println("--- Error occurred: " + e.getMessage());
                return false;
            }
        }

        System.out.print("Enter the Smart Meter Port #: ");
        inStg = input.nextLine();
        if (inStg.length() > 0) {
            try {
                portNum = Integer.parseInt(inStg);
                smartMeter = new InetSocketAddress(meterAddr, portNum);
            } catch (NumberFormatException nfe) {
                System.out.println("--- INVALID PORT: " + inStg);
                System.out.println("--- Error: " + nfe.getMessage());
                return false;
            } catch (IllegalArgumentException iae) {
                System.out.println("--- INVALID PORT: " + inStg);
                System.out.println("--- Error: " + iae.getMessage());
                return false;
            } catch (Exception e) {
                System.out.println("--- Error occurred: " + e.getMessage());
                return false;
            }
        }

        System.out.print("Enter the Smart Meter password (enter for none): ");
        inStg = input.nextLine();
        if (inStg.trim().length() > 0) {
            password = inStg.trim();
        } else {
            password = "";
        }

        System.out.print("Enter the Outstation number (enter for none): ");
        inStg = input.nextLine();
        if (inStg.trim().length() > 0) {
            outstationNum = inStg.trim();
        } else {
            outstationNum = "";
        }

        return true;
    }
        /**
        * Helper method to extract the current date and time out of the byte array
        */
    private static String getClockString(byte[] bytes) {
        int year = ((bytes[0] << 8) & 0xFF00) + (bytes[1] & 0xFF);
        int month = bytes[2];
        int dayOfMonth = bytes[3];
        String dayOfWeek = "";
        switch (bytes[4]) {
            case 1: dayOfWeek = "Monday"; break;
            case 2: dayOfWeek = "Tuesday"; break;
            case 3: dayOfWeek = "Wednesday"; break;
            case 4: dayOfWeek = "Thursday"; break;
            case 5: dayOfWeek = "Friday"; break;
            case 6: dayOfWeek = "Saturday"; break;
            case 7: dayOfWeek = "Sunday"; break;
        }
        int hour = bytes[5];
        int min = bytes[6];
        int sec = bytes[7];
        return dayOfWeek + " " + year + "/" + month + "/" + dayOfMonth + " " + hour
        + ":" + min + ":" + sec;
    }
    private static void printResult(GetResult result) {
        System.out.println("Received data:");
        printData(result.getResultData(), 0);
    }

    private static void printData(Data data, int indent) {
        if (data.getChoiceIndex() == Data.Choices.NULL_DATA) {
            return; // ignore
        }

        for (int i = 0; i < indent; i++) {
            System.out.print("  ");
        }
        if (data.getChoiceIndex() == Data.Choices.ARRAY) {
            System.out.println("[");
            for (Data subData : data.getComplex()) {
                printData(subData, indent + 1);
            }

            for (int i = 0; i < indent; i++) {
                System.out.print("  ");
            }
            System.out.println("]");
        }
        else if (data.getChoiceIndex() == Data.Choices.STRUCTURE) {
            System.out.println("{");
            for (Data subData : data.getComplex()) {
                printData(subData, indent + 1);
            }

            for (int i = 0; i < indent; i++) {
                System.out.print("  ");
            }
            System.out.println("}");
        }
        else if (data.getChoiceIndex() == Data.Choices.LONG_UNSIGNED) {
            System.out.println("Unsigned16: " + data.getNumber());
        }
        else if (data.getChoiceIndex() == Data.Choices.UNSIGNED) {
            System.out.println("Unsigned: " + data.getNumber());
        }
        else if (data.getChoiceIndex() == Data.Choices.OCTET_STRING) {
            System.out.print("Octet String: ");
            for (byte b : data.getByteArray()) {
                String hexString = Integer.toHexString(b & 0xFF);
                hexString = hexString.length() == 1 ? "0" + hexString : hexString;
                System.out.print(hexString + " ");
            }
            System.out.println("");
        }
        else if (data.getChoiceIndex() == Data.Choices.INTEGER) {
            System.out.println("Integer: " + data.getNumber());
        }
        else if (data.getChoiceIndex() == Data.Choices.DOUBLE_LONG_UNSIGNED) {
            System.out.println("Unsigned64: " + data.getNumber());
        }
        else if (data.getChoiceIndex() == Data.Choices.ENUMERATE) {
            System.out.println("Enum: " + data.getNumber());
        }
        else if (data.getChoiceIndex() == Data.Choices.BOOL) {
            System.out.println("Bool: " + data.getBoolean());
        }
        else if (data.getChoiceIndex() == Data.Choices.VISIBLE_STRING) {
            System.out.println("Visible String: " + new String(data.getByteArray(), Charset.forName("US-ASCII")));
        }
        else {
            System.out.println("Unknown Format: " + data.getChoiceIndex());
        }
    }
}
